const os = require('os');
const _ = require('underscore');

const forceArray = source => {
  return _.isArray(source) ? source : [source];
};

class Helper {
  static convert(source) {
    if (!source) return {};

    return _.chain(source)
      .pairs()
      .map(pair => [pair[0], forceArray(pair[1])])
      .object()
      .value();
  }

  static merge(...args) {
    const dist = {};

    for (const item of args.map(Helper.convert)) {
      for (const key of Object.keys(item)) {
        dist[key] = [...(dist[key] || []), ...item[key]];
      }
    }

    return dist;
  }

  static bundleCommand() {
    return os.platform() === 'win32' ? 'bundle.bat' : 'bundle';
  }
}

module.exports = Helper;
