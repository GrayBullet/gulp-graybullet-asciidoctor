const fs = require('fs');
const path = require('path');
const spawn = require('gulp-spawn');
const semver = require('semver');
const addresses = require('email-addresses');
const _ = require('underscore');
const Helper = require('./libs/helper');

const loadFromGemfile = () => {
  if (!fs.existsSync('Gemfile')) {
    return { args: {}, attributes: {}};
  }

  const gemfile = fs.readFileSync('Gemfile', 'utf8').replace('\r', '');
  const lines = gemfile.split('\n');
  const requires = _.chain(lines)
    .filter(line => line.match(/^gem '(asciidoctor-.*)'/))
    .map(line => line.replace(/^gem '(asciidoctor-.*)'/, '$1'))
    .value();
  return { args: { require: requires }, attributes: {}};
};

const loadFromPackageJson = (lang) => {
  const p = JSON.parse(fs.readFileSync('package.json', 'utf8'));

  const opts = { args: {}, attributes: {}};

  if (p.version) {
    opts.attributes.revnumber = p.version;
    opts.attributes.revremark = lang === 'ja' ?
      `第${semver.major(p.version)}.${semver.minor(p.version)}版` :
      `v${semver.major(p.version)}.${semver.minor(p.version)}`;
  }
  if (p.author) {
    const address = addresses.parseOneAddress(p.author);
    opts.attributes.author = address.name;
    opts.attributes.email = address.address;
  }
  if (p.description) opts.attributes.description = p.description;
  if (p.keywords) opts.attributes.keywords = p.keywords.join(',');
  opts.attributes.revdate = `${new Date()}`;

  return opts;
};

const asciidoctor = (options) => {
  const source = [
    asciidoctor.options,
    loadFromGemfile(),
    loadFromPackageJson(options.autoAttributes),
    options || {}
    ];

  const opts = Object.assign({}, ...source);
  const backend = opts.backend || 'html';
  const attributes = Object.assign({}, ...source.map(s => s.attributes || {}));
  const args = Helper.merge(...source.map(s => s.args || {}), {attribute: Object.keys(attributes).map(key => `${key}=${attributes[key]}`)});
  const params = _.chain(args)
    .pairs()
    .map(pair => _.chain(pair[1]).map(v => [`--${pair[0]}`, v]).value())
    .flatten()
    .value();

  return spawn({
    cmd: Helper.bundleCommand(),
    args: ['exec', 'asciidoctor', '-o-', '-', ...params, '--backend', opts.backend],
    filename: base => opts.filename || `${base}.${backend}`,
  })
};

asciidoctor.options = {};

module.exports = asciidoctor;
