# gulp-graybullet-asciidoctor
Ruby AsciiDoctor wrapper gulp plugin.


## Using
```ruby
# Gemfile
source 'https://rubygems.org'

gem 'asciidoctor'

# Additional asciidoctor requires.
gem 'asciidoctor-pdf'
gem 'asciidoctor-pdf-cjk'
gem 'asciidoctor-diagram'
```

```
$ bundle install --path vendor/bundler
$ npm install --save-dev gulp gulp-graybullet-asciidoctor
```

```javascript
// gulpfile.js
const gulp = require('gulp');
const asciidoctor = require('gulp-graybullet-asciidoctor');

// default options.
asciidoctor.options = {
  // Get attribute from package.json. (Ex: author)
  autoAttributes: 'ja',
};

gulp.task('default', () => gulp.src('index.adoc')
  .pipe(asciidoctor({ backend: 'pdf' })))
  .pipe(gulp.dest('dest'));
```


### Options
```javascript
asciidoctor({
  backend: 'pdf', // '--backend pdf'
  autoAttributes: 'ja', // Get attribute from package.json.
  args: { 'base-dir': 'book' }, // '--base-dir book'
  attributes: { doctype: 'book' }, // '--attribute doctype=book'
  filename: 'book.pdf', // Output 'book.pdf'
});
```


## LICENSE
MIT